from setuptools import setup, find_packages

setup(
    name='pmcs',
    version='0.1.0',
    description='Sample code for paging mission control',
    author='Neha Padhi',
    license=license,
    packages=['pmcs', "props"],
    zip_safe=False
)