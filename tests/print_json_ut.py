import unittest
from datetime import datetime as dt
from pmcs import paging_mission_control as pmc


class OutputTestCase(unittest.TestCase):
    def test_output(self):
        ts = "20180101 23:05:07.421"
        tsc = dt.strptime(ts, "%Y%m%d %H:%M:%S.%f")
        expected_output = {
            "satelliteId": 5,
            "severity": "YELLOW LOW",
            "component": "FOOBAR",
            "timestamp": tsc.strftime("%Y-%m-%dT%H:%M:%S.") + ("%0.3f" % (tsc.microsecond / 10 ** 6))[2:] + "Z"
        }
        self.assertEqual(expected_output, pmc.format_output_to_json(5, "YELLOW LOW", "FOOBAR", tsc))


if __name__ == '__main__':
    unittest.main()
