import unittest
from collections import deque
from datetime import datetime as dt
from pmcs import paging_mission_control as pmc


class QMTestCase(unittest.TestCase):

    def test_add(self):
        ts = "20180101 23:05:07.421"
        tsc = dt.strptime(ts, "%Y%m%d %H:%M:%S.%f")
        testDQ = deque([], maxlen=3)
        expectedDQ = deque([tsc], maxlen=3)
        actual = pmc.manage_queue(testDQ, tsc)
        self.assertEqual(expectedDQ, actual)

    def test_max(self):
        tsc1 = dt.strptime("20180101 23:01:09.521", "%Y%m%d %H:%M:%S.%f")
        tsc2 = dt.strptime("20180101 23:01:26.011", "%Y%m%d %H:%M:%S.%f")
        tsc3 = dt.strptime("20180101 23:04:06.017", "%Y%m%d %H:%M:%S.%f")
        tsc4 = dt.strptime("20180101 23:05:07.421", "%Y%m%d %H:%M:%S.%f")

        testDQ = deque([tsc1, tsc2, tsc3], maxlen=3)
        actual = pmc.manage_queue(testDQ, tsc4)
        expectedDQ = deque([tsc2, tsc3, tsc4], maxlen=3)

        self.assertEqual(3, len(actual))
        self.assertEqual(expectedDQ, actual)

    def test_stale1(self):
        tsc1 = dt.strptime("20180101 23:01:09.521", "%Y%m%d %H:%M:%S.%f")
        tsc2 = dt.strptime("20180101 23:07:26.011", "%Y%m%d %H:%M:%S.%f")

        testDQ = deque([tsc1], maxlen=3)
        actual = pmc.manage_queue(testDQ, tsc2)
        expectedDQ = deque([tsc2], maxlen=3)

        self.assertEqual(1, len(actual))
        self.assertEqual(expectedDQ, actual)

    def test_stale2(self):
        tsc1 = dt.strptime("20180101 23:01:09.521", "%Y%m%d %H:%M:%S.%f")
        tsc2 = dt.strptime("20180101 23:02:06.017", "%Y%m%d %H:%M:%S.%f")
        tsc3 = dt.strptime("20180101 23:09:37.421", "%Y%m%d %H:%M:%S.%f")

        testDQ = deque([tsc1, tsc2], maxlen=3)
        actual = pmc.manage_queue(testDQ, tsc3)
        expectedDQ = deque([tsc3], maxlen=3)

        self.assertEqual(1, len(actual))
        self.assertEqual(expectedDQ, actual)

    def test_stale_full(self):
        tsc1 = dt.strptime("20180101 23:01:09.521", "%Y%m%d %H:%M:%S.%f")
        tsc2 = dt.strptime("20180101 23:01:26.011", "%Y%m%d %H:%M:%S.%f")
        tsc3 = dt.strptime("20180101 23:04:06.017", "%Y%m%d %H:%M:%S.%f")
        tsc4 = dt.strptime("20180101 23:09:07.421", "%Y%m%d %H:%M:%S.%f")

        testDQ = deque([tsc1, tsc2, tsc3], maxlen=3)
        actual = pmc.manage_queue(testDQ, tsc4)
        expectedDQ = deque([tsc4], maxlen=3)

        self.assertEqual(1, len(actual))
        self.assertEqual(expectedDQ, actual)

if __name__ == '__main__':
    unittest.main()
