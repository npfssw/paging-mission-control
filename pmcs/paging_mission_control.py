from datetime import datetime as dt
from collections import deque
import os, sys
from props import prop_file

SAT1000 = "1000"
SAT1001 = "1001"
BATT = "BATT"
TSTAT = "TSTAT"
RED_LOW = "RED LOW"
RED_HIGH = "RED HIGH"

max_len = 3

batt1000 = deque([], maxlen=max_len)
tstat1000 = deque([], maxlen=max_len)
batt1001 = deque([], maxlen=max_len)
tstat1001 = deque([], maxlen=max_len)


def paging_mission_control():
    notifications = []
    filepath = prop_file.dir + '/' + prop_file.name

    if not os.path.isfile(filepath):
        print("File path {} does not exist.".format(filepath))
        sys.exit()

    with open(filepath) as fp:
        for line in fp:
            split_line = line.strip().split('|')

            get_violations(split_line[0], split_line[1],
                           int(split_line[2]), int(split_line[5]),
                           float(split_line[6]), split_line[7],
                           notifications)

    print(notifications)


def get_violations(ts, sId, rhl, rll, value, comp, notifications):
    format = "%Y%m%d %H:%M:%S.%f"
    tsc = dt.strptime(ts, format)

    if comp == BATT and value < rll:
        if sId == SAT1000:
            manage_queue(batt1000, tsc)
            if len(batt1000) == max_len:
                output = format_output_to_json(sId, RED_LOW, comp, tsc)
                notifications.append(output)

        else:
            manage_queue(batt1001, tsc)
            if len(batt1001) == max_len:
                output = format_output_to_json(sId, RED_LOW, comp, tsc)
                notifications.append(output)

    elif comp == TSTAT and value > rhl:
        if sId == SAT1000:
            manage_queue(tstat1000, tsc)
            if len(tstat1000) == max_len:
                output = format_output_to_json(sId, RED_HIGH, comp, tsc)
                notifications.append(output)

        else:
            manage_queue(tstat1001, tsc)
            if len(tstat1001) == max_len:
                output = format_output_to_json(sId, RED_HIGH, comp, tsc)
                notifications.append(output)


def manage_queue(q, tsc):
    if len(q) is not 0:
        qcopy = list(q)

        # remove stale data
        for i in range(0, len(qcopy)):
            time_diff = (tsc - qcopy[i]).total_seconds() / 60.0
            if time_diff >= 5.0:
                q.popleft()

        # if queue is still full, evict
        if len(q) == max_len:
            q.popleft()

    q.append(tsc)
    return q


def format_output_to_json(sId, severity, comp, tsc):
    output = {
        "satelliteId": sId,
        "severity": severity,
        "component": comp,
        "timestamp": tsc.strftime("%Y-%m-%dT%H:%M:%S.") + ("%0.3f" % (tsc.microsecond / 10 ** 6))[2:] + "Z"
    }
    return output


if __name__ == '__main__':
    paging_mission_control()
